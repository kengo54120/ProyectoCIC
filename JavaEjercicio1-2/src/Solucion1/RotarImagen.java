/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Solucion1;

/**
 *
 * @author ACL
 */
public class RotarImagen {

    
    boolean rotate(int[][] matrix) {
        /*
        Se realizan los movimientos por secciones es decir ,
        comenzando desde la parte más externa y hacia adentro.
        */
        if (matrix.length == 0 || matrix.length != matrix[0].length) {
            return false;
        }
        int n = matrix.length;

        for (int layer = 0; layer < n / 2; layer++) {
            int first = layer;
            int last = n - 1 - layer;

            for (int i = first; i < last; i++) {
                int offset = i - first;
                int top = matrix[first][i];     // guarda la parte superior
                matrix[first][i] = matrix[last - offset][first]; // se mueve de izquierda hacia arriba
                matrix[last - offset][first] = matrix[last][last - offset]; //se mueve de abajo a la izquierda
                matrix[last][last - offset] = matrix[i][last]; // se mueve de derecha hacia abajo
                matrix[i][last] = top; // guarda la parte superior

            }
        }
        return true;

    }
   
   

}
